=================
*CSV* data reader
=================

*galactic-io-data-csv* is a
`CSV <https://en.wikipedia.org/wiki/Comma-separated_values>`_ data reader plugin
for **GALACTIC**.

The file extension is ``.csv``. The first line of each column contains the names of the
characteristics.
Each new line contains a new individual with its values. For example, an
extraction of
`Fisher's IRIS <https://en.wikipedia.org/wiki/Iris_flower_data_set>`_
data:

.. code-block:: text
    :class: admonition

    "sepal length","sepal width","petal length","petal width","class"
    5.1,3.5,1.4,0.2,Iris-setosa
    4.9,3.0,1.4,0.2,Iris-setosa
    4.7,3.2,1.3,0.2,Iris-setosa
    4.6,3.1,1.5,0.2,Iris-setosa
    5.0,3.6,1.4,0.2,Iris-setosa
    7.0,3.2,4.7,1.4,Iris-versicolor
    6.4,3.2,4.5,1.5,Iris-versicolor
    6.9,3.1,4.9,1.5,Iris-versicolor
    5.5,2.3,4.0,1.3,Iris-versicolor
    6.5,2.8,4.6,1.5,Iris-versicolor
    6.3,3.3,6.0,2.5,Iris-virginica
    5.8,2.7,5.1,1.9,Iris-virginica
    7.1,3.0,5.9,2.1,Iris-virginica
    6.3,2.9,5.6,1.8,Iris-virginica
    6.5,3.0,5.8,2.2,Iris-virginica


This reader uses the ``csv.DictReader`` class of the
`python core library <https://docs.python.org/3/library/csv.html#csv.DictReader>`_.

