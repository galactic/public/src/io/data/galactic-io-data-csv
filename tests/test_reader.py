"""Test CSV reader module."""

from pathlib import Path
from unittest import TestCase

from galactic.io.data.core import PopulationFactory


class CSVDataReaderTest(TestCase):
    def test_get_reader(self):
        pathname = Path(__file__).parent / "test.csv"
        with pathname.open(encoding="utf-8") as data_file:
            population = PopulationFactory.create(data_file)
        self.assertEqual(
            population,
            {
                "0": {"name": "Galois", "firstname": "Évariste"},
                "1": {"name": "Wille", "firstname": "Rudolf"},
            },
        )

    def test_header(self):
        pathname = Path(__file__).parent / "test2.csv"
        with pathname.open(encoding="utf-8") as data_file:
            population = PopulationFactory.create(data_file)
        self.assertEqual(
            population,
            {
                "ÉG": {"name": "Galois", "firstname": "Évariste"},
                "RW": {"name": "Wille", "firstname": "Rudolf"},
            },
        )
