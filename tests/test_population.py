"""Test Population reader module."""

from unittest import TestCase

from galactic.io.data.core import PopulationFactory
from galactic.io.data.csv import CSVDataReader


class PopulationTest(TestCase):
    def test_population(self):
        self.assertTrue(
            any(
                isinstance(reader, CSVDataReader)
                for reader in PopulationFactory.readers()
            ),
        )
