"""
CSV Data reader.
"""

from ._main import CSVDataReader, register

__all__ = ("CSVDataReader", "register")
