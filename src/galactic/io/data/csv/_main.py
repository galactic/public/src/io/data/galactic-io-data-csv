"""
CSV Data reader.
"""

from collections.abc import Iterator, Mapping
from csv import DictReader
from typing import TextIO, cast

from galactic.helpers.core import default_repr
from galactic.io.data.core import PopulationFactory


# pylint: disable=too-few-public-methods
class CSVDataReader:
    """
    CSV Data reader.

    Example
    -------
    >>> from pprint import pprint
    >>> from galactic.io.data.csv import CSVDataReader
    >>> reader = CSVDataReader()
    >>> import io
    >>> data = '''age,height
    ... 46,202
    ... 36,183
    ... 204,230
    ... 36,180
    ... 23,150
    ... 23,172
    ... '''
    >>> data = reader.read(io.StringIO(data))
    >>> pprint(data)
    {'0': {'age': '46', 'height': '202'},
     '1': {'age': '36', 'height': '183'},
     '2': {'age': '204', 'height': '230'},
     '3': {'age': '36', 'height': '180'},
     '4': {'age': '23', 'height': '150'},
     '5': {'age': '23', 'height': '172'}}

    """

    __slots__ = ()

    def __repr__(self) -> str:
        return cast(str, default_repr(self))

    @classmethod
    def read(cls, data_file: TextIO) -> Mapping[str, object]:
        """
        Read a CSV data file.

        Parameters
        ----------
        data_file
            A readable text file.

        Returns
        -------
        Mapping[str, object]
            The data.

        """
        data = DictReader(data_file)
        rows = list(data)
        if data.fieldnames is not None and data.fieldnames[0] == "":
            return {row.pop(""): row for row in rows}
        return {str(index): row for index, row in enumerate(rows)}

    @classmethod
    def extensions(cls) -> Iterator[str]:
        """
        Get an iterator over the supported extensions.

        Returns
        -------
        Iterator[str]
            An iterator over the supported extensions

        """
        return iter([".csv"])


def register() -> None:
    """
    Register an instance of a CSV data reader.
    """
    PopulationFactory.register_reader(CSVDataReader())
